

class NBLabel():

    def __init__(self, label):

        self.label = label
        self.trainingData = []

    def addTrainingData(self, trainingData):

        # Take list of sentences or articles for one particular label
        if type(trainingData) == type([]):

            self.trainingData += trainingData

        elif type(trainingData) == type(str()):

            self.trainingData.append(trainingData)


    def getTrainSet(self):

        return [(td, self.label) for td in self.trainingData]