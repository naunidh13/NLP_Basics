from textblob.classifiers import NaiveBayesClassifier

from NBLabel import NBLabel

class NBClassifier():

    def __init__(self):

        self.model = None
        self.NBLabels = []

    def addLabelData(self, labelName, trainingData):

        nbLabel = NBLabel(labelName)
        nbLabel.addTrainingData(trainingData)
        self.NBLabels.append(nbLabel)

    def train(self):

        if len(self.NBLabels) == 0:
            print "No training data submitted"
            return

        train_set = []

        for nbLabel in self.NBLabels:
            train_set += nbLabel.getTrainSet()

        self.model = NaiveBayesClassifier(train_set)

    def classify(self, inputData):

        if self.model is None:

            print "No model trained"
            return None

        return self.model.classify(inputData)


if __name__ == "__main__":

    modelObj = NBClassifier()
    modelObj.addLabelData("science", ["He likes science fiction", "It's for my science project",
                                      "The science teacher gave Tom an A", "She has to study science"])
    modelObj.addLabelData("lending", ["He established a lending stock to help struggling business men and did much to relieve debtors who had been thrown into prison", "but we are accustomed to transactions of lending and borrowing", "Among lending libraries should be noticed the London Library"])
    modelObj.addLabelData("mortgage", ["instrument evidencing the mortgage", "state of property so mortgaged",
                                       "interest of the mortgagee in such property"])
    modelObj.train()
    print modelObj.classify("a science buff,")
