import re
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize, sent_tokenize


class LabelSet():

    wordnet_lemmatizer = WordNetLemmatizer()

    @staticmethod
    def lemmatize(word):

        lemm_verb = LabelSet.wordnet_lemmatizer.lemmatize(word, pos='n')
        lemm_noun = LabelSet.wordnet_lemmatizer.lemmatize(word, pos='v')

        if len(lemm_verb) < len(lemm_noun):
            return lemm_verb
        else:
            return lemm_noun

    def __init__(self, label):

        self.label = label
        self.data_set = {}


    def normalize(self):

        if len(self.data_set) == 0:

            return

        max_val = self.data_set[max(self.data_set, key=self.data_set.get)]

        if max_val == 2:

            return

        for key in self.data_set:

            self.data_set[key] == 1.0 + float(self.data_set[key]) / float(max_val)



    def vectorize(self, trainingData):

        self.data_set = {}

        if type(trainingData) == type({}):

            for key in trainingData:

                if re.match("[0-9.e+-]+", str(trainingData[key])):

                    if type(trainingData[key]) == type("") or type(trainingData[key]) == type(float()) or type(trainingData[key]) == type(int()):

                        try:

                            self.data_set[key.lower()] = float(trainingData[key])

                        except Exception as e:

                            print "Exception - ", e

                    else:

                        print trainingData[key], " : not a Float or Integer Type"

                else:

                    print trainingData[key], " : not an acceptable data"


        elif type(trainingData) == type([]):

            for word in trainingData:

                wordLemm = LabelSet.lemmatize(word)

                if word.lower() not in self.data_set:

                    self.data_set[word.lower()] = 1

                else:

                    self.data_set[word.lower()] += 1

                if wordLemm.lower() not in self.data_set:

                    self.data_set[wordLemm.lower()] = 1

                else:

                    self.data_set[wordLemm.lower()] += 1

            self.normalize()


        elif type(trainingData) == type(""):

            sents = sent_tokenize(trainingData)

            for sent in sents:

                words = word_tokenize(sent)

                for word in words:

                    wordLemm = LabelSet.lemmatize(word)

                    if word.lower() not in self.data_set:

                        self.data_set[word.lower()] = 1

                    else:

                        self.data_set[word.lower()] += 1


                    if wordLemm.lower() not in self.data_set:

                        self.data_set[wordLemm.lower()] = 1

                    else:

                        self.data_set[wordLemm.lower()] += 1


            self.normalize()


    def getScore(self, inputData):

        score = 0.0
        str_len = 0.1

        if type(inputData) == type(""):

            sents = sent_tokenize(inputData)

            for sent in sents:

                words = word_tokenize(sent)

                for word in words:

                    str_len += 1

                    word = LabelSet.lemmatize(word)

                    if word.lower() in self.data_set:

                        score += self.data_set[word.lower()]

        elif type(inputData) == type([]):

            str_len += len(inputData)

            for word in inputData:

                word = LabelSet.lemmatize(word)

                if word.lower() in self.data_set:

                    score += self.data_set[word.lower()]


        return float(score)/float(str_len)