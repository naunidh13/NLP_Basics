
from LabelSet import LabelSet

class ModelClass():

    def __init__(self):

        self.labelSets = []



    def addLabelSet(self, labelName, trainingData):

        obj = LabelSet(labelName)

        obj.vectorize(trainingData)

        self.labelSets.append(obj)


    def classify(self, inputData):

        maxScore = 0.0
        maxLabel = None

        for labelSet in self.labelSets:

            currScore = labelSet.getScore(inputData)

            if maxScore < currScore:

                maxScore = currScore
                maxLabel = labelSet.label

        return maxLabel, maxScore



if __name__ == "__main__":

    modelObj = ModelClass()
    modelObj.addLabelSet("mortgage", ["risk", "management", "wealth"])
    modelObj.addLabelSet("Lending", ["lend", "borrow", "capital"])
    modelObj.addLabelSet("Science", ["physics", "chemistry", "biology", "study", "semester"])

    print modelObj.classify("lend")